package net.macrochasm.pickuplootfix;

import static org.bukkit.Bukkit.getLogger;

import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PickUpLootFix extends JavaPlugin implements Listener {
    public boolean enforcePickupValue;
    public boolean announceInChat;
    public String announceMsg;
    public boolean debug = true;
    
    @Override
    public void onLoad() {
        FileConfiguration cfg = this.getConfig();
        cfg.addDefault("enforceCanPickUpLootAs", true);
        cfg.addDefault("announceInChat", true);
        cfg.addDefault("announceMsg", "&c%s is an unlucky bastard!");
        cfg.addDefault("debug", false);
        cfg.options().copyDefaults(true);
        this.saveConfig();
        
        this.enforcePickupValue = cfg.getBoolean("enforceCanPickUpLootAs");
        this.announceInChat = cfg.getBoolean("announceInChat");
        this.announceMsg = ChatColor.translateAlternateColorCodes('&', cfg.getString("announceMsg"));
        this.debug = cfg.getBoolean("debug");
        
        getLogger().info(super.getName() + " loaded successfully!");
    }
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this,this);
    }
    @Override
    public void onDisable() {
    
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerJoinEvent(PlayerJoinEvent ev) {
        Player pl = ev.getPlayer();
        boolean canPickup = pl.getCanPickupItems();
        
        if (debug) getLogger().log(
            Level.INFO, String.format("%s: CanPickUpLoot=%b", pl.getDisplayName(), canPickup));
        
        if (canPickup != enforcePickupValue) {
            getLogger().log(Level.WARNING, String.format("Fixing value of CanPickUpLoot for %s: setting to %b", pl.getDisplayName(), enforcePickupValue));
            pl.setCanPickupItems(enforcePickupValue);
            if (announceInChat) {
                Bukkit.broadcastMessage(String.format(announceMsg, pl.getDisplayName()));
            }
        }
    }
}
